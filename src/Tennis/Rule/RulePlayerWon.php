<?php declare(strict_types=1);

namespace Kata\Tennis\Rule;

use Kata\Tennis\Player;

class RulePlayerWon implements RuleInterface
{
    /**
     * @var string
     */
    private $messagePattern;

    /**
     * @param string $messagePattern
     */
    public function __construct(string $messagePattern = 'Winner %s')
    {
        $this->messagePattern = $messagePattern;
    }

    /**
     * @inheritdoc
     */
    public function supportsScore(Player $playerA, Player $playerB): bool
    {
        return ($playerA->getScore() >= 4 && $playerA->getScore() - $playerB->getScore() >= 2)
            || ($playerB->getScore() >= 4 && $playerB->getScore() - $playerA->getScore() >= 2);
    }

    /**
     * @inheritdoc
     */
    public function getScore(Player $playerA, Player $playerB): string
    {
        $winningPlayer = $playerA->getScore() > $playerB->getScore() ? $playerA : $playerB;

        return sprintf($this->messagePattern, $winningPlayer->getName());
    }
}
