<?php declare(strict_types=1);

namespace Kata\Tennis\Rule;

use Kata\Tennis\Player;

class RuleDeuce implements RuleInterface
{
    /**
     * @var string
     */
    private $message;

    /**
     * @param string $message
     */
    public function __construct(string $message = 'Deuce')
    {
        $this->message = $message;
    }

    /**
     * @inheritdoc
     */
    public function supportsScore(Player $playerA, Player $playerB): bool
    {
        return $playerA->getScore() === $playerB->getScore()
            && $playerA->getScore() >= 3;
    }

    /**
     * @inheritdoc
     */
    public function getScore(Player $playerA, Player $playerB): string
    {
        return $this->message;
    }
}
