<?php declare(strict_types=1);

namespace Kata\Tennis\Rule;

use Kata\Tennis\Player;

class RuleAdvantage implements RuleInterface
{
    /**
     * @var string
     */
    private $messagePattern;

    /**
     * @param string $messagePattern
     */
    public function __construct(string $messagePattern = 'Advantage %s')
    {
        $this->messagePattern = $messagePattern;
    }

    /**
     * @inheritdoc
     */
    public function supportsScore(Player $playerA, Player $playerB): bool
    {
        return ($playerA->getScore() >= 3 && $playerB->getScore() === ($playerA->getScore() + 1))
            || ($playerB->getScore() >= 3 && $playerA->getScore() === ($playerB->getScore() + 1));
    }

    /**
     * @inheritdoc
     */
    public function getScore(Player $playerA, Player $playerB): string
    {
        $winningPlayer = $playerA->getScore() > $playerB->getScore() ? $playerA : $playerB;

        return sprintf($this->messagePattern, $winningPlayer->getName());
    }
}
