<?php declare(strict_types=1);

namespace Kata\Tennis\Rule;

use Kata\Tennis\Player;

class RuleNormalPoints implements RuleInterface
{
    /**
     * @var string[] Key = points, value = name
     */
    private $points;

    /**
     * @var string
     */
    private $messagePattern;

    /**
     * @param string $points0
     * @param string $points1
     * @param string $points2
     * @param string $points3
     * @param string $messagePattern
     */
    public function __construct(
        string $points0 = 'Love',
        string $points1 = '15',
        string $points2 = '30',
        string $points3 = '40',
        string $messagePattern = '%s - %s'
    ) {
        $this->points = [
            $points0,
            $points1,
            $points2,
            $points3
        ];
        $this->messagePattern = $messagePattern;
    }

    /**
     * @inheritdoc
     */
    public function supportsScore(Player $playerA, Player $playerB): bool
    {
        return ($playerA->getScore() < 4 && $playerB->getScore() < 3)
            || ($playerB->getScore() < 4 && $playerA->getScore() < 3);
    }

    /**
     * @inheritdoc
     */
    public function getScore(Player $playerA, Player $playerB): string
    {
        $pointsA = $this->points[$playerA->getScore()];
        $pointsB = $this->points[$playerB->getScore()];

        return sprintf($this->messagePattern, $pointsA, $pointsB);
    }
}
