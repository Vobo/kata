<?php declare(strict_types=1);

namespace Kata\Tennis\Rule;

use Kata\Tennis\Player;

interface RuleInterface
{
    /**
     * Whether given rule can generate score for current situation
     *
     * @param Player $playerA
     * @param Player $playerB
     * @return bool
     */
    public function supportsScore(Player $playerA, Player $playerB): bool;

    /**
     * @param Player $playerA
     * @param Player $playerB
     * @return string
     */
    public function getScore(Player $playerA, Player $playerB): string;
}
