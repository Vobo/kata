<?php declare(strict_types=1);

namespace Kata\Tennis;

class Scoreboard
{
    /**
     * @var Rules
     */
    private $rules;

    /**
     * @var Player
     */
    private $playerA;

    /**
     * @var Player
     */
    private $playerB;

    /**
     * @param Rules $ruleSet
     * @param string $namePlayerA
     * @param string $namePlayerB
     */
    public function __construct(Rules $ruleSet, string $namePlayerA, string $namePlayerB)
    {
        $this->rules = $ruleSet;
        $this->playerA = new Player($namePlayerA);
        $this->playerB = new Player($namePlayerB);
    }

    /**
     * @return Player
     */
    public function getPlayerA(): Player
    {
        return $this->playerA;
    }

    /**
     * @return Player
     */
    public function getPlayerB(): Player
    {
        return $this->playerB;
    }

    /**
     * Player A scores a point
     *
     * @return $this
     */
    public function playerAScores(): self
    {
        $this->playerA->scorePoint();

        return $this;
    }

    /**
     * Player B scores a point
     *
     * @return $this
     */
    public function playerBScores(): self
    {
        $this->playerB->scorePoint();

        return $this;
    }

    /**
     * @return string
     */
    public function getScore(): string
    {
        return $this->rules->getScore($this->playerA, $this->playerB);
    }
}
