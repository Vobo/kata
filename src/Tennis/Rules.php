<?php declare(strict_types=1);

namespace Kata\Tennis;

use Kata\Tennis\Rule\RuleInterface;

class Rules
{
    /**
     * @var RuleInterface[]
     */
    private $rules;

    /**
     * @param RuleInterface[] $rules Order = priority
     */
    public function __construct(array $rules)
    {
        if (!$rules) {
            throw new \InvalidArgumentException('At least 1 rule is needed');
        }

        foreach ($rules as $rule) {
            if (!$rule instanceof RuleInterface) {
                throw new \InvalidArgumentException(sprintf(
                    'Expected RuleInterface, got %s',
                    is_object($rule) ? get_class($rule) : gettype($rule)
                ));
            }

            $this->rules[] = $rule;
        }
    }

    /**
     * @param Player $playerA
     * @param Player $playerB
     * @return string
     */
    public function getScore(Player $playerA, Player $playerB)
    {
        foreach ($this->rules as $rule) {
            if ($rule->supportsScore($playerA, $playerB)) {
                return $rule->getScore($playerA, $playerB);
            }
        }

        throw new \LogicException('Invalid scoring rules configuration');
    }
}
