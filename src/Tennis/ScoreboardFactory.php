<?php declare(strict_types=1);

namespace Kata\Tennis;

use Kata\Tennis\Rule\RuleAdvantage;
use Kata\Tennis\Rule\RuleDeuce;
use Kata\Tennis\Rule\RuleNormalPoints;
use Kata\Tennis\Rule\RulePlayerWon;

class ScoreboardFactory
{
    /**
     * @var Rules
     */
    private $rules;

    public function __construct()
    {
        // Normally I'd use a container, but since there is none
        $this->rules = new Rules([
            new RulePlayerWon(),
            new RuleAdvantage(),
            new RuleDeuce(),
            new RuleNormalPoints(),
        ]);
    }

    /**
     * @param string $playerAName
     * @param string $playerBName
     * @return Scoreboard
     */
    public function createScoreboard(string $playerAName, string $playerBName): Scoreboard
    {
        return new Scoreboard($this->rules, $playerAName, $playerBName);
    }
}
