<?php declare(strict_types=1);

namespace Kata\Tennis;

class Player
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $score = 0;

    /**
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Add player a point
     */
    public function scorePoint(): void
    {
        ++$this->score;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
