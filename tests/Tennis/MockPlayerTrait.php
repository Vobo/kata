<?php declare(strict_types=1);

namespace Tests\Tennis;

use Kata\Tennis\Player;
use Prophecy\Exception\Doubler\ClassNotFoundException;
use Prophecy\Exception\Doubler\DoubleException;
use Prophecy\Exception\Doubler\InterfaceNotFoundException;
use Prophecy\Prophecy\ObjectProphecy;

trait MockPlayerTrait
{
    /**
     * @param null|string $classOrInterface
     * @return ObjectProphecy
     * @throws ClassNotFoundException
     * @throws DoubleException
     * @throws InterfaceNotFoundException
     */
    abstract protected function prophesize($classOrInterface = null): ObjectProphecy;

    /**
     * @param int $points
     * @param string $name
     * @return Player
     */
    protected function mockPlayer(int $points = 0, string $name = 'Janek'): Player
    {
        $player = $this->prophesize(Player::class);
        $player->getScore()->willReturn($points);
        $player->getName()->willReturn($name);

        return $player->reveal();
    }
}
