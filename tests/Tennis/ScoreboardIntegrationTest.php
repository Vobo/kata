<?php declare(strict_types=1);

namespace Tests\Tennis;

use Kata\Tennis\Scoreboard;
use Kata\Tennis\ScoreboardFactory;
use PHPUnit\Framework\TestCase;

class ScoreboardIntegrationTest extends TestCase
{
    /**
     * @var Scoreboard
     */
    private $scoreboard;

    public function setUp()
    {
        $factory = new ScoreboardFactory();
        $this->scoreboard = $factory->createScoreboard('Adam', 'Barnaba');
    }

    /**
     * @dataProvider scoringDataProvider
     */
    public function testScoring(int $pointsA, int $pointsB, string $expectedResult)
    {
        for ($i = 0; $i < $pointsA; $i++) {
            $this->scoreboard->playerAScores();
        }

        for ($i = 0; $i < $pointsB; $i++) {
            $this->scoreboard->playerBScores();
        }

        $result = $this->scoreboard->getScore();
        $this->assertEquals($expectedResult, $result);
    }

    public function scoringDataProvider()
    {
        return [
            [0, 0, 'Love - Love'],
            [0, 1, 'Love - 15'],
            [2, 1, '30 - 15'],
            [3, 1, '40 - 15'],
            [4, 2, 'Winner Adam'],
            [3, 4, 'Advantage Barnaba'],
            [6, 6, 'Deuce'],
        ];
    }
}
