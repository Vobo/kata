<?php declare(strict_types=1);

namespace Tests\Tennis;

use Kata\Tennis\ScoreboardFactory;
use PHPUnit\Framework\TestCase;

class ScoreboardFactoryTest extends TestCase
{
    /**
     * @var ScoreboardFactory
     */
    private $factory;

    public function setUp()
    {
        $this->factory = new ScoreboardFactory();
    }

    public function testPassesNames()
    {
        $playerAName = 'player A';
        $playerBName = 'player B';
        $scoreboard = $this->factory->createScoreboard($playerAName, $playerBName);

        $this->assertEquals($playerAName, $scoreboard->getPlayerA()->getName());
        $this->assertEquals($playerBName, $scoreboard->getPlayerB()->getName());
    }
}
