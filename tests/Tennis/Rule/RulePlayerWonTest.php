<?php declare(strict_types=1);

namespace Tests\Tennis\Rule;

use Kata\Tennis\Player;
use Kata\Tennis\Rule\RuleInterface;
use Kata\Tennis\Rule\RulePlayerWon;
use PHPUnit\Framework\TestCase;
use Tests\Tennis\MockPlayerTrait;

class RulePlayerWonTest extends TestCase
{
    use MockPlayerTrait;

    /**
     * @var RuleInterface
     */
    private $rule;

    public function setUp()
    {
        $this->rule = new RulePlayerWon('%s won');
    }

    /**
     * @dataProvider supportsDataProvider
     */
    public function testSupports(Player $playerA, Player $playerB, bool $shouldSupport)
    {
        $result = $this->rule->supportsScore($playerA, $playerB);
        $this->assertEquals($shouldSupport, $result);
    }

    /**
     * Data provider
     */
    public function supportsDataProvider()
    {
        return [
            [$this->mockPlayer(4), $this->mockPlayer(0), true],
            [$this->mockPlayer(4), $this->mockPlayer(1), true],
            [$this->mockPlayer(4), $this->mockPlayer(2), true],

            [$this->mockPlayer(0), $this->mockPlayer(4), true],
            [$this->mockPlayer(1), $this->mockPlayer(4), true],
            [$this->mockPlayer(2), $this->mockPlayer(4), true],

            [$this->mockPlayer(5), $this->mockPlayer(3), true],
            [$this->mockPlayer(6), $this->mockPlayer(4), true],

            [$this->mockPlayer(3), $this->mockPlayer(5), true],
            [$this->mockPlayer(4), $this->mockPlayer(6), true],

            [$this->mockPlayer(4), $this->mockPlayer(3), false],
            [$this->mockPlayer(5), $this->mockPlayer(4), false],
            [$this->mockPlayer(6), $this->mockPlayer(5), false],

            [$this->mockPlayer(3), $this->mockPlayer(4), false],
            [$this->mockPlayer(4), $this->mockPlayer(5), false],
            [$this->mockPlayer(5), $this->mockPlayer(6), false],

            [$this->mockPlayer(0), $this->mockPlayer(6), true],
            [$this->mockPlayer(6), $this->mockPlayer(0), true],
        ];
    }

    /**
     * @dataProvider scoreGenerationDataProvider
     */
    public function testScoreGeneration(Player $playerA, Player $playerB, string $expectedResult)
    {
        $result = $this->rule->getScore($playerA, $playerB);
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Data provider
     */
    public function scoreGenerationDataProvider()
    {
        return [
            [$this->mockPlayer(4, 'Tom'), $this->mockPlayer(1 ), 'Tom won'],
            [$this->mockPlayer(6), $this->mockPlayer(8, 'Lisa' ), 'Lisa won'],
        ];
    }
}
