<?php declare(strict_types=1);

namespace Tests\Tennis\Rule;

use Kata\Tennis\Player;
use Kata\Tennis\Rule\RuleInterface;
use Kata\Tennis\Rule\RuleNormalPoints;
use PHPUnit\Framework\TestCase;
use Tests\Tennis\MockPlayerTrait;

class RuleNormalPointsTest extends TestCase
{
    private const POINTS_0 = 'zero';
    private const POINTS_1 = 'one';
    private const POINTS_2 = 'two';
    private const POINTS_3 = 'three';

    use MockPlayerTrait;

    /**
     * @var RuleInterface
     */
    private $rule;

    public function setUp()
    {
        $this->rule = new RuleNormalPoints(
            self::POINTS_0,
            self::POINTS_1,
            self::POINTS_2,
            self::POINTS_3,
            '%s%s'
        );
    }

    /**
     * @dataProvider supportsDataProvider
     */
    public function testSupports(Player $playerA, Player $playerB, bool $shouldSupport)
    {
        $result = $this->rule->supportsScore($playerA, $playerB);
        $this->assertEquals($shouldSupport, $result);
    }

    /**
     * Data provider
     */
    public function supportsDataProvider()
    {
        return [
            [$this->mockPlayer(1), $this->mockPlayer(2), true],
            [$this->mockPlayer(2), $this->mockPlayer(3), true],
            [$this->mockPlayer(3), $this->mockPlayer(0), true],
            [$this->mockPlayer(3), $this->mockPlayer(1), true],
            [$this->mockPlayer(2), $this->mockPlayer(2), true],
            [$this->mockPlayer(0), $this->mockPlayer(0), true],

            [$this->mockPlayer(4), $this->mockPlayer(0), false],
            [$this->mockPlayer(3), $this->mockPlayer(3), false],
            [$this->mockPlayer(3), $this->mockPlayer(4), false],
            [$this->mockPlayer(4), $this->mockPlayer(3), false],
            [$this->mockPlayer(4), $this->mockPlayer(4), false],
            [$this->mockPlayer(2), $this->mockPlayer(6), false],
        ];
    }

    /**
     * @dataProvider scoreGenerationDataProvider
     */
    public function testScoreGeneration(Player $playerA, Player $playerB, string $expectedScore)
    {
        $result = $this->rule->getScore($playerA, $playerB);
        $this->assertEquals($expectedScore, $result);
    }

    /**
     * Data provider
     */
    public function scoreGenerationDataProvider()
    {
        return [
            [$this->mockPlayer(1), $this->mockPlayer(2), self::POINTS_1.self::POINTS_2],
            [$this->mockPlayer(0), $this->mockPlayer(0), self::POINTS_0.self::POINTS_0],
            [$this->mockPlayer(3), $this->mockPlayer(1), self::POINTS_3.self::POINTS_1],
        ];
    }
}
