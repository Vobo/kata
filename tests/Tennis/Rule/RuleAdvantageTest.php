<?php declare(strict_types=1);

namespace Tests\Tennis\Rule;

use Kata\Tennis\Player;
use Kata\Tennis\Rule\RuleAdvantage;
use Kata\Tennis\Rule\RuleInterface;
use PHPUnit\Framework\TestCase;
use Tests\Tennis\MockPlayerTrait;

class RuleAdvantageTest extends TestCase
{
    use MockPlayerTrait;

    /**
     * @var RuleInterface
     */
    private $rule;

    public function setUp()
    {
        $this->rule = new RuleAdvantage('%s +1');
    }

    /**
     * @dataProvider supportsDataProvider
     */
    public function testSupports(Player $playerA, Player $playerB, bool $shouldSupport)
    {
        $result = $this->rule->supportsScore($playerA, $playerB);
        $this->assertEquals($shouldSupport, $result);
    }

    /**
     * Data provider
     */
    public function supportsDataProvider()
    {
        return [
            [$this->mockPlayer(0), $this->mockPlayer(1), false],
            [$this->mockPlayer(1), $this->mockPlayer(2), false],
            [$this->mockPlayer(2), $this->mockPlayer(3), false],
            [$this->mockPlayer(3), $this->mockPlayer(2), false],

            [$this->mockPlayer(3), $this->mockPlayer(4), true],
            [$this->mockPlayer(4), $this->mockPlayer(5), true],
            [$this->mockPlayer(5), $this->mockPlayer(6), true],

            [$this->mockPlayer(4), $this->mockPlayer(3), true],
            [$this->mockPlayer(5), $this->mockPlayer(4), true],
            [$this->mockPlayer(6), $this->mockPlayer(5), true],

            [$this->mockPlayer(3), $this->mockPlayer(3), false],
            [$this->mockPlayer(4), $this->mockPlayer(4), false],
            [$this->mockPlayer(6), $this->mockPlayer(4), false],
            [$this->mockPlayer(8), $this->mockPlayer(4), false],

        ];
    }

    /**
     * @dataProvider scoreGenerationDataProvider
     */
    public function testScoreGeneration(Player $playerA, Player $playerB, string $expectedResult)
    {
        $result = $this->rule->getScore($playerA, $playerB);
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * Data provider
     */
    public function scoreGenerationDataProvider()
    {
        return [
            [$this->mockPlayer(4,'Andrzej'), $this->mockPlayer(5, 'Krzysztof'), 'Krzysztof +1'],
            [$this->mockPlayer(4,'Andrzej'), $this->mockPlayer(3, 'Krzysztof'), 'Andrzej +1'],
        ];
    }
}
