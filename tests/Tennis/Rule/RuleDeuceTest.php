<?php declare(strict_types=1);

namespace Tests\Tennis\Rule;

use Kata\Tennis\Player;
use Kata\Tennis\Rule\RuleDeuce;
use Kata\Tennis\Rule\RuleInterface;
use PHPUnit\Framework\TestCase;
use Tests\Tennis\MockPlayerTrait;

class RuleDeuceTest extends TestCase
{
    use MockPlayerTrait;

    /**
     * @var RuleInterface
     */
    private $rule;

    public function setUp()
    {
        $this->rule = new RuleDeuce('Draw');
    }

    /**
     * @dataProvider supportsDataProvider
     */
    public function testSupports(Player $playerA, Player $playerB, bool $shouldSupport)
    {
        $result = $this->rule->supportsScore($playerA, $playerB);
        $this->assertEquals($shouldSupport, $result);
    }

    /**
     * Data provider
     */
    public function supportsDataProvider()
    {
        return [
            [$this->mockPlayer(1), $this->mockPlayer(2), false],
            [$this->mockPlayer(2), $this->mockPlayer(1), false],
            [$this->mockPlayer(2), $this->mockPlayer(2), false],

            [$this->mockPlayer(3), $this->mockPlayer(3), true],
            [$this->mockPlayer(4), $this->mockPlayer(4), true],
            [$this->mockPlayer(6), $this->mockPlayer(6), true],

            [$this->mockPlayer(4), $this->mockPlayer(6), false],
            [$this->mockPlayer(6), $this->mockPlayer(4), false],

        ];
    }

    public function testScoreGeneration()
    {
        $result = $this->rule->getScore(
            $this->mockPlayer(3),
            $this->mockPlayer(3 )
        );

        $this->assertEquals('Draw', $result);
    }
}
