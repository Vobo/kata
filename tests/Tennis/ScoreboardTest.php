<?php declare(strict_types=1);

namespace Tests\Tennis;

use Kata\Tennis\Player;
use Kata\Tennis\Rules;
use Kata\Tennis\Scoreboard;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;

class ScoreboardTest extends TestCase
{
    /**
     * @var ObjectProphecy|Rules
     */
    private $rules;

    /**
     * @var Scoreboard
     */
    private $scoreboard;

    public function setUp()
    {
        $this->rules = $this->prophesize(Rules::class);
        $this->scoreboard = new Scoreboard($this->rules->reveal(), 'Andrzej', 'Janusz');
    }

    public function testPlayerAScores()
    {
        $player = $this->scoreboard->getPlayerA();
        $this->assertEquals(0, $player->getScore());

        $this->scoreboard->playerAScores();
        $this->assertEquals(1, $player->getScore());

        $this->scoreboard
            ->playerAScores()
            ->playerAScores();
        $this->assertEquals(3, $player->getScore());
    }

    public function testPlayerBScores()
    {
        $player = $this->scoreboard->getPlayerB();
        $this->assertEquals(0, $player->getScore());

        $this->scoreboard->playerBScores();
        $this->assertEquals(1, $player->getScore());

        $this->scoreboard
            ->playerBScores()
            ->playerBScores();
        $this->assertEquals(3, $player->getScore());
    }

    public function testAsksForScore()
    {
        $this->rules->getScore(Argument::type(Player::class), Argument::type(Player::class))
            ->willReturn('wynik');

        $result = $this->scoreboard->getScore();
        $this->assertEquals('wynik', $result);

        $this->scoreboard
            ->playerAScores()
            ->playerAScores()
            ->playerBScores();

        $result = $this->scoreboard->getScore();
        $this->assertEquals('wynik', $result);
    }
}
