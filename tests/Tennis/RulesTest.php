<?php declare(strict_types=1);

namespace Tests\Tennis;

use Kata\Tennis\Rule\RuleInterface;
use Kata\Tennis\Rules;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class RulesTest extends TestCase
{
    use MockPlayerTrait;

    /**
     * @expectedException \InvalidArgumentException
     * @dataProvider invalidConstructorParamDataProvider
     */
    public function testConstructorAcceptsRulesOnly(array $rules)
    {
        new Rules($rules);
    }

    /**
     * Data provider
     */
    public function invalidConstructorParamDataProvider()
    {
        return [
            [[]],
            [['a']],
            [[10]],
            [[$this->prophesize(RuleInterface::class), new \stdClass()]],
        ];
    }

    /**
     * @expectedException \LogicException
     */
    public function testThrowsExceptionWhenNoRulesSupportScore()
    {
        $rule = $this->prophesize(RuleInterface::class);
        $rule->supportsScore(Argument::any(), Argument::any())->willReturn(false);

        $rules = new Rules([$rule->reveal(), $rule->reveal()]);
        $rules->getScore($this->mockPlayer(), $this->mockPlayer());
    }

    public function testUsesFirstMatchingRule()
    {
        $rules = [];

        for ($i = 0; $i < 3; $i++) {
            $rule = $this->prophesize(RuleInterface::class);
            $rule->supportsScore(Argument::any(), Argument::any())->willReturn($i > 0);
            $rule->getScore(Argument::any(), Argument::any())->willReturn('rule ' . $i);
            $rules[] = $rule->reveal();
        }

        $rules = new Rules($rules);
        $result = $rules->getScore($this->mockPlayer(), $this->mockPlayer());

        $this->assertEquals('rule 1', $result);
    }
}
