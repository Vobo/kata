<?php declare(strict_types=1);

namespace Tests\Tennis;

use Kata\Tennis\Player;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase
{
    private const NAME = 'Andrzej';

    /**
     * @var Player
     */
    private $player;

    public function setUp()
    {
        $this->player = new Player(self::NAME);
    }

    public function testInitialValues()
    {
        $this->assertEquals(self::NAME, $this->player->getName());
        $this->assertEquals(0, $this->player->getScore());
    }

    public function testScoring()
    {
        for ($i = 0; $i < 7; $i++) {
            $this->assertEquals($i, $this->player->getScore());
            $this->player->scorePoint();
        }
    }
}
